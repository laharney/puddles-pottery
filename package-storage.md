{
  "name": "puddles-pottery",
  "version": "1.0.0",
  "private": true,
  "description": "puddles-pottery",
  "author": "Jack Glenn",
  "keywords": [
    "gatsby"
  ],
  "scripts": {
    "develop": "gatsby develop",
    "start": "gatsby develop",
    "build": "gatsby build",
    "serve": "gatsby serve",
    "clean": "gatsby clean"
  },
  "dependencies": {
    "dotenv": "^16.3.1",
    "gatsby": "^5.11.0",
    "gatsby-plugin-image": "^3.11.0",
    "gatsby-plugin-netlify-cms": "^7.11.0",
    "gatsby-plugin-sharp": "^5.11.0",
    "gatsby-source-filesystem": "^5.11.0",
    "gatsby-source-instagram-all": "^5.2.4",
    "gatsby-transformer-sharp": "^5.11.0",
    "lodash": "^4.17.21",
    "netlify-cms-app": "^2.15.72",
    "react": "^18.2.0",
    "react-dom": "^18.2.0"
  }
}
