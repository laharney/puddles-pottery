/**
 * @type {import('gatsby').GatsbyConfig}
 */

require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  siteMetadata: {
    title: `Puddles Pottery`,
    siteUrl: `https://www.puddlespottery.com`,
    description: `Puddles Pottery - Handmade Functional Ceramics`,
    keywords: `Puddles Pottery, ceramics, pottery, handmade, art`
  },
  flags: {
    DEV_SSR: true
  },
  plugins: ["gatsby-plugin-netlify", "gatsby-plugin-netlify-cms", "gatsby-plugin-image", "gatsby-plugin-sharp", "gatsby-transformer-sharp", {
    resolve: 'gatsby-source-filesystem',
    options: {
      "name": "images",
      "path": "./src/images/"
    },
    __key: "images"
  }, {
    resolve: `gatsby-source-instagram-all`,
    options: {
      access_token: process.env.GATSBY_INSTAGRAM_TOKEN,
    }
  },
]
};
