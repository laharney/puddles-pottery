import React, { useState } from 'react';
import '../styles/css/style.css';

const About = () => {
  const [isStoryDropdownOpen, setIsStoryDropdownOpen] = useState(true);
  const [isCreationDropdownOpen, setIsCreationDropdownOpen] = useState(true);

  const toggleStoryDropdown = () => {
    setIsStoryDropdownOpen(!isStoryDropdownOpen);
  };

  const toggleCreationDropdown = () => {
    setIsCreationDropdownOpen(!isCreationDropdownOpen);
  };

  return (
    <section id="about">
      <div className="about-container">
        <div className="about-title">
          <h2>About Me</h2>
        </div>
        <div className="about-content">
          <div className="about-dropdowns">
              <div className="about-story">
                  <div className={`about-story-link ${isStoryDropdownOpen ? 'active' : ''}`} onClick={toggleStoryDropdown}>
                      <div className="about-button"><strong>Story</strong></div>
                  </div>
                  {isStoryDropdownOpen && (
                  <div className="about-story-dropdown-content">
                      <p >I started working with clay in 2018 and quickly fell in love!
                        I'm hoping to build this into a self-sustaining hobby and small business.</p>

                        <p>I chose the name Puddles Pottery because I've been using various forms
                          of the username “JumpinPuddles” for nearly 20 years now. It seemed like
                          the right alliterative next step.</p>
                  </div>
              )}

              </div>
              <div className="about-creation-process">

                  <div className={`about-creation-process-link ${isCreationDropdownOpen ? 'active' : ''}`} onClick={toggleCreationDropdown}>
                      <div className="about-button"><strong>Creation Process</strong></div>
                  </div>
                  {isCreationDropdownOpen && (
                      <div className="about-story-dropdown-content">
                          <p>
                            Overall, the pottery process takes about a month because there is a lot
                            of waiting for things to dry or heat properly! Beyond the waiting, it
                            takes several hours to make each individual piece. From weighing the
                            initial clay to sanding down burrs to measuring glaze, there's a lot
                            of detail work involved. Finally, each piece I carve or test special
                            glazes on gets an extra hour or more of attention.
                          </p>
                          <p>
                            <a
                            className="creation-process-dropdown-link"
                            href="https://potterycrafters.com/the-7-stages-of-clay/"
                            target="_blank">
                              Here's a webpage
                            </a>
                            &nbsp;that describes the process from Pottery Crafters. Please let me know if you are confused or
                            have questions about the process or time, but I promise - I dedicate as much time as possible
                            to each piece I make.
                          </p>
                      </div>
                  )}

            </div>
          </div>
          <div className="about-image-wrapper">
            <div className="about-image"></div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;
