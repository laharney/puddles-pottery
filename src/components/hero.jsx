import React from 'react'
import '../styles/css/style.css'
// import 'react-bootstrap-icons'


const Hero = () => {
    return (
        <section id="hero" className="hero-container">
            <div className="hero-logo">

            </div>
            {/* <div className="hero-title">
                <h1>Puddles Pottery</h1>
            </div> */}
            <div className="hero-image">

            </div>
            <div className="hero-bottom-spacer"></div>
        </section>
    )
}

export default Hero
