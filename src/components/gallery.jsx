import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'
import _get from 'lodash/get'


const Gallery = () => {
  const data = useStaticQuery(graphql`
    query InstagramQuery {
      allInstagramContent {
        edges {
          node {
            localImage {
              childImageSharp {
                gatsbyImageData(layout: FIXED, width: 200, height: 200)
              }
            }
          }
        }
      }
    }
  `)


  // modify this function for how many images you want to display
  const arrayOfInstaImages = _get(data, 'allInstagramContent.edges', [])
  const firstTwelveImages = arrayOfInstaImages.slice(0,12)


  return (
    <section id="gallery">
      <div className="gallery-container">
        <div className="gallery-title">
          <h2>Gallery</h2>
        </div>
        <div className="gallery-content">
          {firstTwelveImages.map(({ node }, i) => {
            const image = getImage(node.localImage.childImageSharp.gatsbyImageData)

            // return (
            //   <div key={i} style={{ width: "200px", height: "200px" }}>
            //     <GatsbyImage image={image} alt="Description" />
            //   </div>
            // )
            return (
              <div key={i} className="image-wrapper">
                <GatsbyImage image={image} alt="Instagram Images from Puddles Pottery" />
              </div>
            )
          })}
        </div>
      </div>
    </section>
  )
}

export default Gallery
