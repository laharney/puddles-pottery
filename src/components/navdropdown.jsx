import React, { useState } from 'react'
import '../styles/css/style.css'

const NavDropdown = () => {

    const [activeLink, setActiveLink] = useState('#hero')
    const [isDropdownOpen, setIsDropdownOpen] = useState(false)

    const handleLinkClick = (link, event) => {
    event.preventDefault();
    setActiveLink(link);

    const target = document.querySelector(link);
    const headerHeight = document.getElementById('header').offsetHeight;
    const targetOffset = target.offsetTop - headerHeight - 7.5; // Subtracting 7.5vh from the target's offset
    window.scrollTo({
      top: targetOffset,
      behavior: 'smooth',
    });
    }


    const toggleDropdown = () => {
        setIsDropdownOpen(!isDropdownOpen);
    }

return (

<div className="nav-dropdown-link-container">
    <div className="">
    <a
      href="#hero"
      data-target="#hero"
      className={`nav-dropdown-link scrollto ${activeLink === '#hero' ? 'active' : ''}`}
      onClick={(event) => handleLinkClick('#hero', event)}
    >
      <span>Home</span>
    </a>
  </div>
  <div className="">
    <a
      href="#about"
      data-target="#about"
      className={`nav-dropdown-link scrollto ${activeLink === '#about' ? 'active' : ''}`}
      onClick={(event) => handleLinkClick('#about', event)}
    >
      <span>About</span>
    </a>
  </div>

  <div className="">
    <a
      href="#gallery"
      data-target="#gallery"
      className={`nav-dropdown-link scrollto ${activeLink === '#gallery' ? 'active' : ''}`}
      onClick={(event) => handleLinkClick('#gallery', event)}
    >
      <span>Gallery</span>
    </a>
  </div>

  <div className="">
    <a
      href="#shop"
      data-target="#shop"
      className={`nav-dropdown-link scrollto ${activeLink === '#shop' ? 'active' : ''}`}
      onClick={(event) => handleLinkClick('#shop', event)}
    >
      <span>Shop</span>
    </a>
  </div>

  <div className="">
    <a
      href="#faq"
      data-target="#faq"
      className={`nav-dropdown-link scrollto ${activeLink === '#faq' ? 'active' : ''}`}
      onClick={(event) => handleLinkClick('#faq', event)}
    >
      <span>FAQ</span>
    </a>
  </div>

  <div className="">
    <a
      href="#contact"
      data-target="#contact"
      className={`nav-dropdown-link scrollto ${activeLink === '#contact' ? 'active' : ''}`}
      onClick={(event) => handleLinkClick('#contact', event)}
    >
      <span>Contact</span>
    </a>
  </div>
</div>

)

}

export default NavDropdown
