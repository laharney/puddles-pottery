import React from 'react'
import { StaticImage } from 'gatsby-plugin-image'
import '../styles/css/style.css'
// import 'react-bootstrap-icons'


const Shop = () => {
    return (
        <section id="shop">
            <div className="shop-container">
                <div className="shop-title">
                    <h2>Shops</h2>
                </div>
                <div className="shop-stores-container">
                    <div className="online-stores-container">
                        <div className="online-store-title">
                            <h3>Online Stores</h3>
                        </div>
                            <div className="shop-card-container">
                                <div className="online-store-card">
                                    <a
                                    className="local-store-card-image etsy-container"
                                    href="https://www.etsy.com/shop/PuddlesPottery"
                                    target="_blank"
                                    >
                                        <div className="etsy-image"></div>
                                    </a>

                                </div>

                            <div className="online-store-card">
                                {/* <div className="local-store-card-title">
                                    Balefire Goods
                                </div> */}
                                <a
                                 className="local-store-card-image balefire-goods-container"
                                 href="https://balefiregoods.com/collections/puddles-pottery"
                                 target="_blank"
                                 >
                                    <div className="balefire-goods-image"></div>
                                </a>
                            </div>

                        </div>
                    </div>
                    <div className="local-stores-container">
                        <div className="local-store-title">
                            <h3>Local Denver Stores</h3>
                        </div>
                        <div className="shop-card-container">
                            <div className="local-store-card">
                                {/* <div className="local-store-card-title">
                                    Fern and Skye
                                </div> */}
                                <div className="local-store-card-image fern-and-skye-container">

                                <a
                                className="fern-and-skye-image"
                                href="https://fernandskye.com/"
                                target="_blank">
                                            <StaticImage
                                            src="../styles/img/FernAndSkye.PNG"
                                            alt="Fern and Skye shop logo"
                                            placeholder="blurred"
                                            layout="fixed"
                                            />

                                </a>
                                </div>
                            </div>

                            <div className="local-store-card">
                                {/* <div className="local-store-card-title">
                                    Balefire Goods
                                </div> */}
                                <a
                                 className="local-store-card-image balefire-goods-container"
                                 href="https://balefiregoods.com/collections/puddles-pottery"
                                 target="_blank"
                                 >
                                    <div className="balefire-goods-image"></div>
                                </a>
                            </div>
                            <div className="local-store-card">
                                {/* <div className="local-store-card-title">
                                    Tigerlily Goods
                                </div> */}
                                <div className="local-store-card-image tigerlily-container">
                                <a
                                className=""
                                href="https://www.tigerlilygoods.com/products-gifts"
                                target="_blank"
                                >
                                            <StaticImage
                                            src="../styles/img/TigerlilyGoods.PNG"
                                            alt="Fern and Skye shop logo"
                                            placeholder="blurred"
                                            layout="constrained"
                                            />
                                </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="upcoming-markets-container">
                        <div className="market-title">
                            <h3>Upcoming Markets</h3>
                        </div>
                        <div className="shop-card-container">
                                <div className="market-card">
                                    <a
                                    className="local-store-card-image etsy-container"
                                    href="https://ndkdenver.org/"
                                    target="_blank"
                                    >
                                        <div className="market-1-image"></div>
                                    </a>

                                </div>

                            {/* <div className="market-card">
                                <div className="market-card-title">
                                    Market 2 Placeholder
                                </div>
                                <div className="market-card-image">

                                </div>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Shop
