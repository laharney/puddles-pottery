import React, { useState } from 'react'
import '../styles/css/style.css'

//This is the FAQ component
const Faq = () => {
    const [isFaq1Open, setIsFaq1Open] = useState(false) // Setting initial state as 'false' so the dropdows are not open on load
    const [isFaq2Open, setIsFaq2Open] = useState(false)
    const [isFaq3Open, setIsFaq3Open] = useState(false)
    const [isFaq4Open, setIsFaq4Open] = useState(false)
    const [isFaq5Open, setIsFaq5Open] = useState(false)
    const [isFaq6Open, setIsFaq6Open] = useState(false)
    const [isFaq7Open, setIsFaq7Open] = useState(false)
    const [isFaq8Open, setIsFaq8Open] = useState(false)

    const toggleFaq1Dropdown = () => {  // logic for opening/closing dropdowns
        setIsFaq1Open(!isFaq1Open);
    }
    const toggleFaq2Dropdown = () => {
        setIsFaq2Open(!isFaq2Open);
    }
    const toggleFaq3Dropdown = () => {
        setIsFaq3Open(!isFaq3Open);
    }
    const toggleFaq4Dropdown = () => {
        setIsFaq4Open(!isFaq4Open);
    }
    const toggleFaq5Dropdown = () => {
        setIsFaq5Open(!isFaq5Open);
    }
    const toggleFaq6Dropdown = () => {
        setIsFaq6Open(!isFaq6Open);
    }
    const toggleFaq7Dropdown = () => {
        setIsFaq7Open(!isFaq7Open);
    }
    const toggleFaq8Dropdown = () => {
        setIsFaq8Open(!isFaq8Open);
    }

    // FAQ JSX section
    return (
        <section id="faq" >
            <div className="faq-container">
                <div className="faq-title">
                    <h2>FAQ</h2>
                </div>
                <div className="faq-body">
                    <div className="faq-content">
                        <div className="faq-element">
                            <div className="faq-1-link" >
                                <div className="faq-button" onClick={toggleFaq1Dropdown}>&gt;&nbsp;Do you take custom orders?</div>
                            </div>
                            {/* hidden below here is the FAQ 1 text information, and so forth for the others */}
                            {isFaq1Open && (
                                <div className="faq-dropdown-content">
                                    <p>
                                        Definitely maybe! Asking for a custom piece is a pretty dangerous topic.
                                        I will happily send you plenty of examples as to why that is, but really -
                                        I can do EVERYTHING in my power to make something perfect, but I never
                                        know what will come out of the kiln!
                                    </p>
                                    <p>
                                        Please feel free to send me a message and we can chat about your ideas.
                                        However, keep in mind, to make ONE custom piece I may have to make 2-3
                                        versions of the same item to get one to you. This is fairly standard to
                                        the entire ceramics industry and should explain why so few ceramic
                                        artists make custom pieces!
                                    </p>
                                    <p>
                                        For an example - please look at the before-kiln and after-kiln images of&nbsp;
                                        <a className="faq-dropdown-link" href="https://www.instagram.com/p/B8mKJpwn14n/?igshid=1qynkzoe3vy86" target="_blank">this beautiful mug.</a>
                                    </p>

                                </div>
                            )}
                        </div>
                        <div className="faq-element">
                            <div className="faq-2-link">
                                <div className="faq-button" onClick={toggleFaq2Dropdown}>&gt;&nbsp;Where can I shop?</div>
                            </div>
                            {isFaq2Open && (
                                <div className="faq-dropdown-content">
                                    <p>Thank you! &#x1F609; Pieces are for sale at the following two sites:</p>
                                    <a className="faq-dropdown-link"href="https://www.tigerlilygoods.com/products-gifts" target="_blank">Tiger Lily Goods</a>
                                    <p></p>
                                    <a className="faq-dropdown-link"href="https://balefiregoods.com/collections/puddles-pottery" target="_blank">Balefire Goods</a>
                                </div>
                            )}
                        </div>
                        <div className="faq-element">
                            <div className="faq-3-link">
                                <div className="faq-button" onClick={toggleFaq3Dropdown}>&gt;&nbsp;How do I care for my new ceramic artwork?</div>
                            </div>
                            {isFaq3Open && (
                                <div className="faq-dropdown-content">
                                    <p>
                                        I try very hard to make sure all my pieces are ready for everyday functional
                                        use. But please remember, ceramics are fragile! Please handle each piece with
                                        care, ESPECIALLY around carvings, lids, or handles. This might be vitrified
                                        stoneware, but I but polst of small details into my pieces. Even if you're not
                                        taking my pieces camping or letting a toddler toss them around, they would
                                        appreciate a little extra love. If you're patient enough, please just hand-wash
                                        the pieces &#40;but don't pour boiling water directly on them&#41; and try to
                                        avoid giving them a thermal shock. Thermal shock is a rapid expansion or
                                        contraction of material following a drastic temperature change...like freezing
                                        pottery, or pouring boiling water on or into it.
                                    </p>
                                </div>
                            )}
                        </div>
                        <div className="faq-element">
                            <div className="faq-4-link">
                                <div className="faq-button" onClick={toggleFaq4Dropdown}>&gt;&nbsp;Are the pieces microwave or dishwasher-safe?</div>
                            </div>
                            {isFaq4Open && (
                                <div className="faq-dropdown-content">
                                    <p>
                                        Yes! All my work is microwave and dishwasher safe unless stated otherwise,
                                        which usually only occurs for custom pink-painted pieces. Of course be careful,
                                        your mug IS stoneware and it may be very hot when it comes out!
                                    </p>
                                    <p>
                                        However, like all nice ceramics, try to avoid giving them a thermal shock
                                        via the microwave or pouring boiling water directly into them. You CAN do
                                        these things, but it's not going to improve the longevity of your piece.
                                        The same goes for the dishwasher: you can do it, but it's best to avoid if
                                        possible.
                                    </p>
                                </div>
                            )}
                        </div>
                        <div className="faq-element">
                            <div className="faq-5-link">
                                <div className="faq-button" onClick={toggleFaq5Dropdown}>&gt;&nbsp;I bought multiples, will you combine shipping?</div>
                            </div>
                            {isFaq5Open && (
                                <div className="faq-dropdown-content">
                                    <p>
                                        Yes! I will combine shipping and refund applicable shipping overages.
                                        Please be aware that multiple ceramic pieces in one box means more
                                        packaging material which creates more weight resulting in a higher cost.
                                        I put a low estimate of shipping costs and often end up paying some extra
                                        out of my pocket with every shipment.
                                    </p>
                                </div>
                            )}
                        </div>
                        <div className="faq-element">
                            <div className="faq-6-link">
                                <div className="faq-button" onClick={toggleFaq6Dropdown}>&gt;&nbsp;"Seconds" Sale Pieces</div>
                            </div>
                            {isFaq6Open && (
                                <div className="faq-dropdown-content">
                                    <p>
                                        If I mark a piece as "Second" it has a small imperfection. I'll photograph
                                        and describe the imperfection in the item's body, and know that it almost
                                        always will be a aesthetic rather than functional issue such as an out-of-place
                                        glaze drip. Usually, it's a completely "non-issue" like a pinhole in the glaze
                                        where two components didn't melt in a way that completely covers the surface, or
                                        the silica melted too much such that the glaze had to be sanded down on the
                                        bottom of the piece*. Please remember that all my ceramic art is handmade and
                                        may have some nuances which give it that handmade touch - if anything is wrong,
                                        I will note that.
                                    </p>
                                    <p>
                                        * Glaze colors are legitmate chemistry an it's <i>so</i> frustrating to have a
                                        beautiful painting destroyed because some elements burn away at higher
                                        temperatures or the silica slightly overheats and melts.
                                    </p>
                                </div>
                            )}
                        </div>
                        <div className="faq-element">
                            <div className="faq-7-link">
                                <div className="faq-button" onClick={toggleFaq7Dropdown}>&gt;&nbsp;Packaging</div>
                            </div>
                            {isFaq7Open && (
                                <div className="faq-dropdown-content">
                                    <p>
                                        I try my best to pack each item carefully to ensure its safe arrival using
                                        recyclable packaging materials! If you do get plastic material in your box,
                                        these materials were from other packages or donated to me and are being
                                        re-used! As an avid environmentalist, I ask you to please reuse or recycle
                                        all packaging material to the best of your ability as well.
                                    </p>
                                </div>
                            )}
                        </div>
                        <div className="faq-element">
                            <div className="faq-8-link">
                                <div className="faq-button" onClick={toggleFaq8Dropdown}>&gt;&nbsp;Exchanges and Returns</div>
                            </div>
                            {isFaq8Open && (
                                <div className="faq-dropdown-content">
                                    <p>
                                        Unfortunately I don't have the ability to offer exchanges or returns at
                                        this time. Remember that each piece is handmade and as such will have
                                        customizations, personalizations, and nuances. I put a lot of work into every
                                        piece that I make and really don't have the ability to support exchanges or returns,
                                        especially with the cost and risk of shipping both ways. To prevent the desire
                                        for return,s I do my best to photograph everything in bright natural light. Please
                                        note that glaze colors may differ in dim artificial lighting.
                                    </p>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className="faq-image">

                    </div>
                </div>

            </div>
        </section>
    )
}

export default Faq
