import React, { useState } from 'react'
import '../styles/css/style.css'

const SocialDropdown = () => {


return (
            <div className="soc-dropdown-link-container">
                <div className="">
                    <a
                    href="https://www.instagram.com/puddlespottery/?hl=en"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="soc-dropdown-link"
                    >
                    <span>Instagram</span>
                    </a>
                </div>
                <div className="">
                    <a
                    href="https://www.etsy.com/shop/puddlespottery"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="soc-dropdown-link"
                    >
                    <span>Etsy</span>
                    </a>
                </div>
            </div>
    )
}

export default SocialDropdown
