import React from 'react'
import '../styles/css/style.css'


const Contact = () => {
    return (
        <section id="contact">
            <div className="contact-container">
                <div className="contact-title">
                    <h2>Contact</h2>
                </div>
                <div className="contact-content">
                <div className="contact-left-div">
                    <div className="contact-image"></div>
                </div>
                <div className="contact-right-div">
                    <div className="contact-info">
                        <div className="contact-info-card">
                            <strong>Location:</strong> Denver, CO
                        </div>
                        <div className="contact-info-card">
                            <strong>Email:</strong> puddles.pottery@gmail.com
                        </div>
                    </div>
                    <div className="form-container">
                        <div className="contact-form-title">
                            <h3>Send Me A Message!</h3>
                        </div>

                        <form name="contact" method="POST" data-netlify="true">
                            <p>
                                <label>Your Name: <input type="text" name="name" /></label>
                            </p>
                            <p>
                                <label>Your Email: <input type="email" name="email" /></label>
                            </p>
                            <p>
                                <label>Message: <textarea name="message"></textarea></label>
                            </p>
                            <p>
                                <button type="submit">Send</button>
                            </p>
                        </form>

                    </div>
                </div>
                </div>
            </div>
        </section>
    )
}

export default Contact
