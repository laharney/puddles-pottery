import React, { useState, useEffect, useRef } from 'react'
import '../styles/css/style.css'
import NavDropdown from './navdropdown'
import SocialDropdown from './socialdropdown'

// import 'react-bootstrap-icons'


const Header = () => {

    const [activeLink, setActiveLink] = useState('#hero')
    const [isNavDropdownOpen, setIsNavDropdownOpen] = useState(false)
    const [isSocDropdownOpen, setIsSocDropdownOpen] = useState(false)
    const dropdownNavMenuRef = useRef(null)
    const dropdownSocMenuRef = useRef(null)


    const handleLinkClick = (link, event) => {
    event.preventDefault();
    setActiveLink(link);

    const target = document.querySelector(link);
    const headerHeight = document.getElementById('header').offsetHeight;
    const targetOffset = target.offsetTop - headerHeight - 7.5; // Subtracting 7.5vh from the target's offset
    window.scrollTo({
      top: targetOffset,
      behavior: 'smooth',
    });
    }

    const toggleNavDropdown = (event) => {
        event.stopPropagation();
        setIsNavDropdownOpen(!isNavDropdownOpen);
    }

    useEffect(() => {
        const handleClickOutsideNav = (event) => {
            if (
                dropdownNavMenuRef.current &&
                !dropdownNavMenuRef.current.contains(event.target) &&
                event.target.closest('main')
            ) {
                setIsNavDropdownOpen(false);
            }
        };

        window.addEventListener('click', handleClickOutsideNav);
        return() => {
            window.removeEventListener('click', handleClickOutsideNav);
        }
    }, []);

    const toggleSocDropdown = (event) => {
        event.stopPropagation();
        setIsSocDropdownOpen(!isSocDropdownOpen);
    }

    useEffect(() => {
        const handleClickOutsideSoc = (event) => {
            if (
                dropdownSocMenuRef.current &&
                !dropdownSocMenuRef.current.contains(event.target) &&
                event.target.closest('main')
            ) {
                setIsSocDropdownOpen(false);
            }
        };

        window.addEventListener('click', handleClickOutsideSoc);
        return() => {
            window.removeEventListener('click', handleClickOutsideSoc);
        }
    }, []);



    return (
        <section id="header">
            <div className="nav-link-container">
                <div className="nav-btn nav-home-btn">
                    <a
                    href="#hero"
                    data-target="#hero"
                    className={`nav-link scrollto ${activeLink === '#hero' ? 'active' : ''}`}
                    onClick={(event) => handleLinkClick('#hero', event)}
                    >
                        <span>Home</span>
                    </a>
                </div>
                <div className="nav-btn nav-about-btn">
                    <a
                    href="#about"
                    data-target="#about"
                    className={`nav-link scrollto ${activeLink === '#about' ? 'active' : ''}`}
                    onClick={(event) => handleLinkClick('#about', event)}
                    >
                        <span>About</span>
                    </a>
                </div>
                <div className="nav-btn nav-gallery-btn">
                    <a
                    href="#gallery"
                    data-target="#gallery"
                    className={`nav-link scrollto ${activeLink === '#gallery' ? 'active' : ''}`}
                    onClick={(event) => handleLinkClick('#gallery', event)}
                    >
                        <span>Gallery</span>
                    </a>
                </div>
                <div className="nav-btn nav-shop-btn">
                    <a
                    href="#shop"
                    data-target="#shop"
                    className={`nav-link scrollto ${activeLink === '#shop' ? 'active' : ''}`}
                    onClick={(event) => handleLinkClick('#shop', event)}
                    >
                        <span>Shop</span>
                    </a>
                </div>
                <div className="nav-btn nav-faq-btn">
                    <a
                    href="#faq"
                    data-target="#faq"
                    className={`nav-link scrollto ${activeLink === '#faq' ? 'active' : ''}`}
                    onClick={(event) => handleLinkClick('#faq', event)}
                    >
                        <span>FAQ</span>
                    </a>
                </div>
                <div className="nav-btn nav-contact-btn">
                    <a
                    href="#contact"
                    data-target="#contact"
                    className={`nav-link scrollto ${activeLink === '#contact' ? 'active' : ''}`}
                    onClick={(event) => handleLinkClick('#contact', event)}
                    >
                        <span>Contact</span>
                    </a>
                </div>
                        {/* Nav Dropdown Menu */}
                <div className={`nav-dropdown-btn ${isNavDropdownOpen ? 'dropdown-active' : ''}`}>

                <div className="dropdown-trigger" onClick={toggleNavDropdown}>
                    <span className="nav-dropdown-label">Nav</span>
                    <span className="nav-dropdown-icon">{isNavDropdownOpen ? ' ▲' : ' ▼'}</span>
                </div>
                {isNavDropdownOpen && (
                    <div className={`nav-dropdown-menu ${isNavDropdownOpen ? 'show' : ''}`} ref={dropdownNavMenuRef}>
                        <NavDropdown/>
                    </div>
                )}
                </div>
            </div>
            <div className="soc-link-container">
                <div className="soc-btn instagram-link-btn">
                    <a
                    href="https://www.instagram.com/puddlespottery/?hl=en"
                    target="_blank"
                    className="soc-link">
                        Insta
                    </a>
                </div>
                <div className="soc-btn etsy-link-btn">
                    <a
                    href="https://www.etsy.com/shop/PuddlesPottery"
                    target="_blank"
                    className="soc-link">
                        Etsy
                    </a>
                </div>
                            {/* Social Dropdown Menu */}
                <div className={`soc-dropdown-btn ${isSocDropdownOpen ? 'dropdown-active' : ''}`}>

                <div className="soc-dropdown-trigger" onClick={toggleSocDropdown}>
                    <span className="soc-dropdown-label">Links</span>
                    <span className="soc-dropdown-icon">{isSocDropdownOpen ? ' ▲' : ' ▼'}</span>
                </div>
                {isSocDropdownOpen && (
                    <div className={`soc-dropdown-menu ${isSocDropdownOpen ? 'show' : ''}`} ref={dropdownSocMenuRef}>
                        <SocialDropdown/>
                    </div>
                )}
                </div>
            </div>
        </section>
    )
}

export default Header
