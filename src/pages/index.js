import * as React from "react"
import Header from '../components/header'
import Hero from '../components/hero'
import About from '../components/about'
import Gallery from "../components/gallery"
import Shop from "../components/shop"
import Faq from "../components/faq"
import Contact from "../components/contact"
import Footer from "../components/footer"



const IndexPage = () => {
  return (
    <>
    <main >
      <Header/>
      <Hero/>
      <About/>
      <Gallery/>
      <Shop/>
      <Faq/>
      <Contact/>
      <Footer/>
    </main>
  </>
  )
}


export default IndexPage

export const Head = () => <title>Puddles Pottery</title>
